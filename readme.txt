JsParser.Cmd. Command line tool to verify javascript code. Based on visual studio extension: JavascriptParser.
To use it, you need to specify the next arguments:
-include:*.js           - path masks to include (divided by ';'), this tool can proccess files with the next extensions: js, htm, html, aspx, asp, ascx, master, cshtml
-exclude:*.min.js       - path masks to exclude (divided by ';')(optional)
-directory:c:\myFiles\  - target directory with your source files.
