﻿using System;
using System.IO;
using System.Linq;
using JsParser.Core.Parsers;

namespace JsParser.Cmd.Code
{
    class FileProcessor
    {
        public bool Process(string filePath)
        {
            var settings = new JavascriptParserSettings { Filename = Path.GetFileName(filePath) };
            var jsParser = new JavascriptParser(settings);
            var results = jsParser.Parse(File.ReadAllText(filePath));
            if (!(results.Errors.Any() || (results.InternalErrors != null && results.InternalErrors.Any())))return true;
            Console.WriteLine(filePath);
            foreach (var error in results.Errors)
            {
                Console.WriteLine("[{0},{1}] Error: {2}", error.StartLine, error.StartColumn, error.Message);
            }
            if (results.InternalErrors != null)
            {
                foreach (var error in results.InternalErrors)
                {
                    Console.WriteLine("[{0},{1}] InternalError: {2}", error.StartLine, error.StartColumn, error.Message);
                }
            }
            return false;
        }
    }
}
