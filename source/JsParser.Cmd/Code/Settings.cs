﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsParser.Cmd.Code
{
    class Settings
    {
        public string Include { get; set; }
        public string Exclude { get; set; }
        public string WorkingDirectory { get; set; }
    }
}
