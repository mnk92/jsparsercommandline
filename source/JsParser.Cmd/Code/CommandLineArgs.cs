﻿namespace JsParser.Cmd.Code
{
    static class CommandLineArgs
    {
        public const string Include = "-include:";
        public const string Exclude = "-exclude:";
        public const string WorkingDirectory = "-directory:";
    }
}
