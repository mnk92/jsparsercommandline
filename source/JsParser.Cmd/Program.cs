﻿using System;
using JsParser.Cmd.Code;

namespace JsParser.Cmd
{
    class Program
    {
        static int Main(string[] args)
        {
            var result = 0;
            try
            {
                var parser = new CommandLineArgumentsParser();
                var processor = new FileProcessor();
                var generator = new FilePathsGenerator();
                foreach (var path in generator.GetFiles(parser.Parse(args)))
                {
                    if (!processor.Process(path))
                        result = 1;
                }
            }
            catch (CommandLineArgumentException)
            {
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                result = -1;
            }
            return result;
        }
    }
}
